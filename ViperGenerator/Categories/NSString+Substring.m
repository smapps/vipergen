//
//  NSString+Substring.m
//  ViperGenerator
//
//  Created by Sameh Mabrouk on 2/17/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "NSString+Substring.h"

@implementation NSString (Substring)

-(NSRange)findLastOccurrenceOfSubString:(NSString*)subString
{
    return [self rangeOfString:subString options:NSBackwardsSearch];
}

-(NSString *)replaceACharacterAtRange:(NSRange)range byCharacters:(NSString*)stringContainingAChars
{
    return [self stringByReplacingCharactersInRange:range withString:stringContainingAChars];
}

@end
