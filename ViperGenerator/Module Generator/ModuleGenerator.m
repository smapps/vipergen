//
//  ModuleGenerator.m
//  ViperGenerator
//
//  Created by Sameh Mabrouk on 2/1/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "ModuleGenerator.h"
#import "Constants.h"
#import "NSString+Substring.h"

@implementation ModuleGenerator

-(id)init{
    self = [super init];
    if (self) {
        self.fileManager = [[FileManager alloc] init];
        self.filesUtilObj = [[FilesUtils alloc] init];
    }
    return self;
}

-(void)doIT
{
    NSLog(@"doIT");
}

/*!
 @brief It generate Viper Module file structure.
 @discussion generateViperModuleWith: Generate Viper Module file structure based on passed Viper module name, author name, company, language Swift/Objective C, path to save module and Viper template
 @param  name The Viper module name.
 @param  projectName The project name.
 @param  author The class author name.
 @param  company The App company name.
 @param  path The path to save the generated module.
 @param  language The App language to generate the Viper module based on it.
 @param  viperTemplate The Viper module template.
 @return void
 */
-(void)generateViperModuleWithName:(NSString *)viperModuleName projectName:(NSString *)projectName author:(NSString *)author company:(NSString *)company path:(NSString *)path language:(NSString *)language viperTemplate:(NSString *)viperTemplate callback:(callback_block)callback
{
    //1- path_from get viper template path based on passed language and template name.
    NSString *sourcePath = [self.fileManager getSourcePath:viperTemplate language:language];

    //2- path_to get the destination path
    NSString *destinationPath = [self.fileManager getDestinationPath:path viperModuleName:viperModuleName];

    //3- copy viper files from source to destination
    [self.fileManager copyEntryFrom:sourcePath pathTo:destinationPath callback:^(BOOL success, NSError *error) {

        if (success) {
            //4- get files from destination directory
            NSArray *files = [self.fileManager deepSeachFilesAtPath:destinationPath];

            //5- rename content of all files like name, author, company and class name.
            [self renameFiles:files name:viperModuleName projectName:projectName author:author company:company];
        }
        
        callback(success, error);
        NSLog(@"copy Error %@", error);
    }];



}

/*!
 @brief Renames all files in the passed files array.
 @param  files files array to be renamed.
 @param  name The Viper module name.
 @param  projectName The name of the project entered by the end user.
 @param  author The class author name.
 @param  company The App company name.
 */
-(void)renameFiles:(NSArray*)files name:(NSString*)name projectName:(NSString *)projectName author:(NSString*)author company:(NSString*)company
{
    //call rename file for each file in files array.
    for (NSString *file in files) {
        NSLog(@"file to be renamed %@", file);
        [self renameFile:file name:name projectName:projectName author:author company:company];
    }

}

/*!
 @brief Renames a given file name and the content of the file.
 @param  fileName The file name to be renamed.
 @param  name The Viper module name.
 @param  author The class author name.
 @param  company The App company name.
 */
-(void)renameFile:(NSString*)filePath name:(NSString*)name projectName:(NSString *)projectName author:(NSString*)author company:(NSString*)company
{
    NSRange range = [filePath findLastOccurrenceOfSubString:kVIPERMODULENAMETOBEREPLACED];
    NSString *newPath= [filePath replaceACharacterAtRange:range byCharacters:name];
    NSLog(@"new path %@", newPath);

    //call move file from FileManager to rename a given file.
    [self.fileManager moveFileAtPath:filePath toPath:newPath];

    //call rename file content to rename content of given file.
    [self renameFileContent:newPath name:name projectName:projectName author:author company:company];
}

/*!
 @brief Renames a file content.
 @param  fileName The file name to be renamed.
 @param  name The Viper module name.
 @param  author The class author name.
 @param  company The App company name.
 */
-(void)renameFileContent:(NSString*)filePath name:(NSString*)name  projectName:(NSString *)projectName author:(NSString*)author company:(NSString*)company
{
    // Read file content.
    NSString *fileContent = [self.filesUtilObj readFileContentAtPath:filePath];

    // Replace content.
    fileContent = [fileContent stringByReplacingOccurrencesOfString:kFILENAME withString:name];
    fileContent = [fileContent stringByReplacingOccurrencesOfString:kPROJECTNAME withString:projectName];
    fileContent = [fileContent stringByReplacingOccurrencesOfString:KFULLUSERNAME withString:author];
    fileContent = [fileContent stringByReplacingOccurrencesOfString:KDATE withString:[self currentDate:[NSDate date]]];
    fileContent = [fileContent stringByReplacingOccurrencesOfString:kYEAR withString:[self currentYear:[NSDate date]]];
    fileContent = [fileContent stringByReplacingOccurrencesOfString:kORGANIZATION withString:company];
    fileContent = [fileContent stringByReplacingOccurrencesOfString:kVIPERMODULENAMETOBEREPLACED withString:name];

    // Save content with replaced string.
    [self.filesUtilObj writeContents:fileContent ToFileAtPath:filePath];
}

/*!
 @brief Simple function that convert current NSDate to NSString.
 @param  date Current Date.
 @return NSString Current date as string.
 */
-(NSString*)currentDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    return [dateFormatter stringFromDate:date];
}

/*!
 @brief Simple function that get current year as string from current date.
 @param  date Current Date.
 @return NSString Current year as string.
 */
-(NSString*)currentYear:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    return [formatter stringFromDate:date];
}

@end
