//
//  TemplateManager.m
//  ViperGenerator
//
//  Created by Sameh Mabrouk on 2/13/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "TemplateManager.h"

@implementation TemplateManager

-(id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

-(NSString*)getTemplateDirectory
{
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSLog(@"bundle Path %@", bundlePath);
    NSLog(@"new path %@", [bundlePath stringByAppendingString:@"/Contents/Resources/Templates/objc/DataManager/"]);

    NSError *error = nil;
    NSArray *cont =   [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[bundlePath stringByAppendingString:@"/Contents/Resources/Templates/objc/DataManager/"] error:&error];
    NSLog(@"files %@", cont);

    return [bundlePath stringByAppendingString:@"/Contents/Resources/Templates/"];
}

-(NSArray *)getTemplates
{
    FileManager *fileManager = [[FileManager alloc] init];
    NSString *templateDir = [self getTemplateDirectory];
    return [fileManager filesAtPath:templateDir];
}

@end