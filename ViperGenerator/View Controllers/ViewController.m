//
//  ViewController.m
//  ViperGenerator
//
//  Created by Sameh Mabrouk on 2/1/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "ViewController.h"
#import "ModuleGenerator.h"
#import "FilesUtils.h"
#import "TemplateManager.h"
#import "Constants.h"
#import "FileManager.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.generatedModuleButton setEnabled:NO];
    
    self.ProjectNameTextField.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

-(IBAction)createModule:(id)sender
{
    NSLog(@"createModule");

    //Generate Class
    self.moduleGenerator = [[ModuleGenerator alloc] init];
//    [self.moduleGenerator generateViperModuleWithName:@"List" projectName:@"ThatCopy" author:@"Sameh Mabrouk" company:@"smapps" path:self.modulePathTexField.stringValue language:@"objc" viperTemplate:@"objc" callback:^(BOOL success, NSError *error) {
    [self.moduleGenerator generateViperModuleWithName:self.moduleNameTextField.stringValue projectName:self.ProjectNameTextField.stringValue author:@"Sameh Mabrouk" company:self.companyTextField.stringValue path:self.modulePathTexField.stringValue language:self.languagesPopUpButton.titleOfSelectedItem viperTemplate:@"objc" callback:^(BOOL success, NSError *error) {

        if (error) {
            if (error.code == 516) {
                //Handle Copying Error: because an item with the same name already exists.
                [self displayPopupAlert:@"Module couldn't be created becasue it's already exists. Do you want to replace it with the new one?"];
            }
        }
    }];
}

/*!
 @brief Create and display Alert sheet.  The functionality of Alert Sheets is the same like Alert Panel, but instead of being a floating window the Alert Sheet is attached to your main application window.
 @param message The alert’s informative text.
 */
-(void)displayPopupAlert:(NSString*)message
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@""];
    [alert setInformativeText:message];
    [alert addButtonWithTitle:@"Replace"];
    [alert addButtonWithTitle:@"Stop"];

    //for displaying Alert Panel.
    //    alert.delegate = self;
    //    [alert runModal];

    [alert beginSheetModalForWindow:self.view.window completionHandler:^(NSModalResponse returnCode) {
        // check returnCode here
        NSLog(@"returnCode %ld", (long)returnCode);
        if (returnCode == NSModalResponseStop) {
            //Handling Replace case
        }
        else if (returnCode == NSModalResponseAbort) {
            //Handling Stop case

        }

    }];

}

/*
 - (BOOL)textShouldBeginEditing:(NSText *)textObject
 {

 return true;
 }
 */

#pragma mark - NSControlSubclassNotifications
- (void)controlTextDidChange:(NSNotification *)notification {
    NSLog(@"controlTextDidChange");

    NSTextField *textField = [notification object];
    if ([textField doubleValue] < 0 | [textField doubleValue] > 255) {
        textField.textColor = [NSColor redColor];
    }


    NSOpenPanel* openDlg = [NSOpenPanel openPanel];

    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];

    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:YES];

    // Change "Open" dialog button to "Select"
    [openDlg setPrompt:@"Select"];

    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSModalResponseOK )
    {
        NSURL *result = openDlg.URL;
        if (result != nil) {
            NSString *path = result.path;
            self.modulePathTexField.stringValue = path;

            //check if all textfields contain stringValue or not to enable Generate button.
            [self.generatedModuleButton setEnabled:YES];
        }
        else{
            // User clicked on cancel
            return;
        }

    }

}

- (void)controlTextDidEndEditing:(NSNotification *)notification {
    NSLog(@"controlTextDidEndEditing");
    NSTextField *textField = [notification object];
    if ([textField resignFirstResponder]) {
        textField.textColor = [NSColor blackColor];
    }
}

- (void)controlTextDidBeginEditing:(NSNotification *)notification
{
    NSLog(@"textDidBeginEditing");
    
}

@end
